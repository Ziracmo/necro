import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainComponent } from './main.component';
import {NbLayoutModule, NbMenuModule, NbSidebarModule} from '@nebular/theme';
import {RouterModule} from '@angular/router';
import {HomeModule} from '../../pages/home/home.module';
import {StuffsModule} from '../../pages/stuffs/stuffs.module';
import {BuildModule} from '../../pages/build/build.module';

@NgModule({
  declarations: [MainComponent],
  imports: [
    CommonModule,
    HomeModule,
    StuffsModule,
    BuildModule,
    NbLayoutModule,
    NbSidebarModule,
    NbMenuModule,
    RouterModule
  ]
})
export class MainModule { }
