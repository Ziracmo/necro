import {Component, OnInit} from '@angular/core';
import {NbMenuItem} from '@nebular/theme';
import {NavigationStart, Router} from '@angular/router';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {

  public items: NbMenuItem[] = [
    {
      title: 'Accueil',
      icon: 'home-outline',
      link: '/home',
    },
    {
      title: 'Equipements',
      icon: 'folder-outline',
      link: '/stuffs',
    },
    {
      title: 'Créer un build',
      icon: 'cube-outline',
      link: '/build',
    },
  ];

  constructor(private router: Router) {
    this.router.events.subscribe(event => {
      if (event instanceof NavigationStart) {
        this.setSelectedPage(event.url)
      }
    })
  }

  ngOnInit(): void {
  }

  private setSelectedPage(route: string) {
    this.items.map(v => {
      v.selected = route.includes(v.link);
      return v;
    })
  }


}
