import {Component, OnInit} from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import {WakfuService} from './core/services/wakfu/wakfu.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  constructor(
    private translate: TranslateService,
    private wakfuService: WakfuService,
  ) {
    translate.setDefaultLang('en');
  }

  ngOnInit(): void {
    this.wakfuService.getConfig().subscribe(console.log)
  }
}
