import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {HomeComponent} from './home.component';
import {NbCardModule, NbIconModule, NbLayoutModule, NbMenuModule, NbSidebarModule} from "@nebular/theme";

@NgModule({
  declarations: [HomeComponent],
  imports: [
    CommonModule,
    NbCardModule,
    NbMenuModule,
    NbIconModule,
    NbLayoutModule,
    NbSidebarModule
  ]
})
export class HomeModule {
}
