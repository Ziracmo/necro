import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {PageNotFoundComponent} from './page-not-found.component';
import {NbButtonModule, NbThemeModule} from '@nebular/theme';

@NgModule({
  declarations: [PageNotFoundComponent],
  imports: [
    CommonModule,
    NbThemeModule.forRoot({name: 'dark'}),
    NbButtonModule,
  ]
})
export class PageNotFoundModule {

}
