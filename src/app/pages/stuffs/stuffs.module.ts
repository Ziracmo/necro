import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StuffsComponent } from './stuffs.component';



@NgModule({
  declarations: [StuffsComponent],
  imports: [
    CommonModule
  ]
})
export class StuffsModule { }
