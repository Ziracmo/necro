import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {PageNotFoundComponent} from './pages/page-not-found/page-not-found.component';
import {MainComponent} from './templates/main/main.component';
import {HomeComponent} from './pages/home/home.component';
import {BuildComponent} from './pages/build/build.component';
import {StuffsComponent} from './pages/stuffs/stuffs.component';

const routes: Routes = [
  {
    path: '',
    component: MainComponent,
    children: [
      {
        path: 'home',
        component: HomeComponent
      },
      {
        path: 'stuffs',
        component: StuffsComponent
      },
      {
        path: 'build',
        component: BuildComponent
      },
      {
        path: '**',
        component: PageNotFoundComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule {}
