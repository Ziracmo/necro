import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {AppConfig} from '../../../../environments/environment';
import {Observable} from 'rxjs';
import {WakfuConfigResponse} from '../../models/wakfu.model';

// INFO https://www.wakfu.com/fr/forum/467-developpement/416762-donnee-json

@Injectable({
  providedIn: 'root'
})
export class WakfuService {

  constructor(private httpClient: HttpClient) { }

  public getConfig(): Observable<WakfuConfigResponse> {
    return this.httpClient.get<WakfuConfigResponse>(AppConfig.wakfuUrl + 'config.json');
  }
}
