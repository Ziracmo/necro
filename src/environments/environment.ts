export const AppConfig = {
  production: false,
  environment: 'LOCAL',
  wakfuUrl: 'https://wakfu.cdn.ankama.com/gamedata/'
};
